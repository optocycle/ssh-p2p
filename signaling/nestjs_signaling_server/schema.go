package http-signaling

// URI default signaling server
const URI = "https://optocycle.xite.dev/ice-signaling/"

// ConnectInfo SDP by offer or answer
type ConnectInfo struct {
	Source string `json:"source"`
	SDP    string `json:"sdp"`
}
