
import { Body, Controller, Get, HttpCode, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { Response } from 'express';


class MessageQueue {
  protected _messages: { [key: number]: any } = {};
  protected _removeHandles: { [key: number]: any } = {};
  protected _listeners: { [key: number]: any } = {};

  constructor(public readonly messageTimeout: number, public readonly maxMessages: number=Infinity) {}

  set(key: string, data: any): void {
    if (Object.keys(this._messages).length > this.maxMessages && !this.has(key))
      throw new Error('too many messages')
    // save new message
    this._messages[key] = data
    // schedule removal
    this._removeHandles[key] = setTimeout(() => this._remove(key), this.messageTimeout);
    // notify listener
    const listener = this._listeners[key];
    if (listener)
      listener();
  }

  get(key: string): any | undefined {
    const message = this._messages[key]
    if (message) {
      this._remove(key)
      return message
    }
  }

  async getWait(key: string, timeout: number): Promise<any | undefined> {
    // return immediately if we have a messsage for key
    if (this.has(key))
      return this.get(key);
    // set up listener
    const messagePromise = new Promise((resolve) => { this._listeners[key] = () => resolve(true); });
    const timeoutPromise = new Promise((resolve) => setTimeout(() => resolve(false), timeout));
    if (await Promise.race([messagePromise, timeoutPromise])) {
      // got new message
      return this.get(key);
    } else {
      // clean up listener
      delete this._listeners[key]
    }
  }

  has(key: string): boolean {
    return !!this._messages[key];
  }

  protected _remove(key: string): void {
    console.log('removing message', key)
    if (this._messages[key])
      delete this._messages[key]
    if (this._removeHandles[key])
      delete this._removeHandles[key]
    if (this._listeners[key])
      delete this._listeners[key]
    console.log('messages remaining:', Object.keys(this._messages).length)
  }
}


@Controller()
export class AppController {
  protected messages = new MessageQueue(60_000, 1000);

  @Post('/push/:key')
  @HttpCode(HttpStatus.OK)
  pushUser(@Param('key') key: string, @Body() data: any, @Res({passthrough: true}) resp: Response): void {
    console.log('push:', key);
    console.log('<< received', data);
    this.messages.set(key, data);
  }

  @Get('/pull/:key')
  async returnUser(@Param('key') key: string, @Res({passthrough: true}) resp: Response): Promise<any> {
    console.log('pull:', key);
    const message = await this.messages.getWait(key, 10_000);
    if (message) {
      console.log('>> sending', message);
      return message;
    }
    resp.status(HttpStatus.NO_CONTENT);
  }
}
