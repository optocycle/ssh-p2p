# ssh-p2p
ssh p2p tunneling server and client. Include new signaling server.

# connection sequence

1. ssh ---dial---> ssh-p2p client
2. ssh-p2p client <----negotiation----> ssh-p2p server
3. sshd <--dial--- ssh-p2p server

# backend protocol

- RTCDataChannel/WebRTC: https://github.com/pions/webrtc

thx! https://github.com/pions/webrtc

# install

1. clone the repo /optocycle/ssh-p2p to your local directory
2. inside the repo: $ go build -o bin/ssh-p2p
3. create a config.yaml within the bin/ directory

## config

the script works with a config.yaml file.
Within this file you make all the configuration you need to.
The structure should be like:

key: uuid
dial_address: 127.0.0.1:dial_port(use 22 as default)
listen_address: 127.0.0.1:listen_port(use 2222 as default)
stun-server: stun:yourstunserver:5349
turn-server: turn:yourstunserver:3478
turn-username: username_turn
turn-password: password_turn
signaling_url: url_to_the_signaling_server

# usage

## server side (the device you want to access)

```sh
$ ./bin/ssh-p2p server -config=path_to_config.yaml
```

## client side

```sh
$ ./bin/ssh-p2p client -config=path_to_config.yaml
```

## client side other terminal

```sh
$ ssh -p listen_port 127.0.0.1
```

**connect to server side sshd !!**
